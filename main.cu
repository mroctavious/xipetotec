#include "src/HCAudioTools.h"

int main( int argc, char *argv[] )
{
	//Encrypt
	if( argc == 3 ){
		encryptParallelProc( argv[1], argv[2] );
		encryptSeqProc( argv[1], argv[2] );
	}
	//Decrypt
	else if( argc == 4 )
	{
		decryptParallelProc( argv[1], argv[2], argv[3] );
		decryptSeqProc( argv[1], argv[2], argv[3] );

	}
	return 0;
}
