#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>

//Recursive definition of determinate using expansion by minors.
int Determinant(int **a,int n);

//Find the cofactor matrix of a square matrix
void CoFactor(int **a,int n,int **b);

//Quasirandom numbers
int qRandom();

//Greatest common divisor, to verify if it is a compatible key
int gcd(long long int, int );

//Transpose of a square matrix, do it in place
void Transpose(int **a,int n);


//Euclidan module
int mymod (long long int n, int m);

//This function will calculate if its an integer number
bool isInt(double number);

//Modular Multiplicative Inverse,
	///////////////////////////////////////
	//FORMULA//////////////////////////////
	//	X=( ( MODULE * i) + 1 ) / R  //
	///////////////////////////////////////
int modular_inverse_multiplicative(int determinant, int mod);

//Aplicar modulo a cada posicion de la llave
int *applyModular(int **a, int n, int det, int mod);

//Conseguimos la inversa modular de una llave dada
int *getInverseKey( int **key, int size, int mod );

//Consigue una llave aleatoria compatible con el algoritmo hill cipher
int **getRandomKey( int **key, int size, int mod );

//Crea llave aleatoria este es un wrapper de getRandomKey, para solo pedir 2 parametros
//y que la otra funcion no reserve mucha memoria
int **createRandomKey( int size, int mod );

//Imprimir vector 1D
void printV( int *vect, int size );

//Imprimir matrix 2D
void printM( int **mat, int size );

//Libera matriz
void freeMat( int **key, int size );

//Funcion que aplica la matriz A a la llave B
void matrixMult8( long long unsigned int *C, uint8_t *B, int *A, int N );

//Funcion que aplica la matriz A a la llave B
//void matrixMult16(int *C, uint16_t *B, int *A, int N, int mod );
void matrixMult16(long long unsigned int *C, uint16_t *B, int *A, int N, int mod);

//Convert a matrix to a vector, CAUTION: this function free's
//the first argument given(matrix), you can restore the key back to
//2D with the function key1Dto2D()
int *key2Dto1D( int **matrix, int size);

//Transform the key to another dimesion
int **key1Dto2D( int *vector, int size);

#include "matrixHCFunctions.c"
