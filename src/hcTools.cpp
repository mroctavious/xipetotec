#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//Funcion que aplica la matriz A a la llave B
void matrixMult(int *C, uint8_t *B, int *A, int N )
{
	int i,j;
	//Multiply the key
	for(i = 0; i < N; ++i)
	{
		for(j = 0; j < N; ++j)
		{
				C[i] += A[i*N+j] * B[j];
		}
	}
}

void printM(int *key, int n)
{
	int i,j;
	for( i=0; i<n; i++)
	{
		for( j=0; j<n; j++ )
		{
			printf("%d ", key[i*n+j]);
		}
		printf("\n");
	}
}

