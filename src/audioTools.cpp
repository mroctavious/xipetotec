#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <inttypes.h>
#include <string.h>
#include "hcTools.cpp"
#include "matrixHCFunctions.h"
#include "crypp.c"
#define KEY_DEFAULT_SIZE 14
#define MOD 256

//Compiling g++ -std=c++11
using std::cin;
using std::cout;
using std::endl;
using std::fstream;
using std::string;

typedef struct  WAV_HEADER
{
    //RIFF Chunk Descriptor
    uint8_t         RIFF[4];        // RIFF Header Magic header
    uint32_t        ChunkSize;      // RIFF Chunk Size
    uint8_t         WAVE[4];        // WAVE Header

    //FMT sub-chunk
    uint8_t         fmt[4];         // FMT header
    uint32_t        Subchunk1Size;  // Size of the fmt chunk
    uint16_t        AudioFormat;    // Audio format 1=PCM,6=mulaw,7=alaw,     257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
    uint16_t        NumOfChan;      // Number of channels 1=Mono 2=Sterio
    uint32_t        SamplesPerSec;  // Sampling Frequency in Hz
    uint32_t        bytesPerSec;    // bytes per second
    uint16_t        blockAlign;     // 2=16-bit mono, 4=16-bit stereo
    uint16_t        bitsPerSample;  // Number of bits per sample

    //Data sub-chunk
    uint8_t         Subchunk2ID[4]; // "data"  string
    uint32_t        Subchunk2Size;  // Sampled data length
} wav_hdr;


// Function prototypes
int getFileSize(FILE* inFile);
void writeAudioFile( const char *outName, int8_t *data, wav_hdr *header, uint64_t samples );
int8_t *modifyAudioData( uint8_t *data, uint64_t samples, int *key, uint16_t keySize );

int getKeySize( uint64_t nSamples, int keySize )
{
	if( keySize < 3 )
		return KEY_DEFAULT_SIZE;
	else
		if( nSamples % keySize == 0 )
			return keySize;
		else
			return getKeySize( nSamples, keySize - 1 );
}

//Check if the string has the extension
int itHasExt( char *filename, const char *ext )
{
	//Verify if the file has the .wav extension
	int continueFlag=0;

	//Get the size of the string, we will check +4 positions
	int len=strlen(filename);

	//Position and confirmed
	int pos=0, i, confirmed=0;

	//Extension to check
	int extLen=strlen(ext);

	//Recorrer el nombre de archivo en busca de la extension
	for( i=len-4; i<len; i++ )
	{
		//Check if the character is the same in the extension
		if( filename[i] == ext[pos] )
		{
			++confirmed;
		}
		++pos;
	}

	//If all ext characters were found
	if( confirmed == extLen )
		return 1;

	//The extension was not found
	else
		return 0;
}



int encryptSeqProc( char *filename, char *outFilename )
{
	//Reserve memory for a struct header
	wav_hdr wavHeader;

	//Get header from binary file
	int headerSize = sizeof(wav_hdr), filelength = 0;

	//The name of the wav file
	char *wavFilename=NULL;

	//Temporal wav file
	char preout[2048];
	strcpy( preout, filename );
	strcat( preout, ".tmp");

	//Create the command for ffmpeg, this will help to make a generic input for the algorithm
	char command[3000];
	strcpy(command, "ffmpeg -y -ar 44100 -acodec pcm_u8 -ab 192k -f wav ");
	strcat(command, preout );
	strcat(command, "  -i ");
	strcat( command, filename );
	system( command );

	//Preout is the temporal wav file, it will be removed later
	wavFilename=preout;

	//Try to open the wav file
	FILE* wavFile = fopen(wavFilename, "r");
	if (wavFile == nullptr)
	{
		fprintf(stderr, "Unable to open wave file: %s\n", filename);
		return 1;
	}

	//Read the header
	size_t bytesRead = fread(&wavHeader, 1, headerSize, wavFile);

	//If there are bytes in the file
	if (bytesRead > 0)
	{
		//Number of buffers used
		int currentStep=0;

		//Read the data
		uint16_t bytesPerSample = wavHeader.bitsPerSample / 8;      //Number     of bytes per sample

		//Total number of samples
		uint64_t numSamples = wavHeader.ChunkSize / bytesPerSample; //How many samples are in the wav file?

		//Size of the key
		int keySize=getKeySize(numSamples, KEY_DEFAULT_SIZE);

		//Refill in case the size of the key does not fit
		if( numSamples % keySize != 0 )
		{
			numSamples=(keySize-(numSamples % keySize) ) + numSamples;
		}

		//Create a quasirandom key
		int **keyMatrix=createRandomKey(keySize, MOD);

		//Transform the 2D key to a single big vector
		int *key=key2Dto1D(keyMatrix, keySize);

		//Get key size based on the fill size of the samples
		static const uint16_t BUFFER_SIZE = 4096;

		//Tamano de las muestras
		uint64_t nSamples=0;
		if( numSamples % keySize != 0 )
		{
			nSamples=(keySize-(numSamples % keySize) ) + numSamples;
		}
		else
		{
			nSamples=numSamples;
		}

		//Reservar memoria para los datos del audio
		uint8_t *audioData = (uint8_t*)malloc( sizeof(uint8_t) * bytesPerSample * nSamples);

		//Avoid memory problems, set all to 0(clean)
		memset( audioData, 0, sizeof(uint8_t) * nSamples );

		//Buffer para ir guardando los datos de audio
		int8_t* buffer =  (int8_t*)malloc(sizeof(int8_t) * BUFFER_SIZE);

		//Store data
		while (  ( bytesRead = fread(buffer, sizeof buffer[0], BUFFER_SIZE / (sizeof buffer[0]), wavFile)  ) > 0  )
		{
			for( int i=0; i<bytesRead; i++ )
			{
				audioData[i+(BUFFER_SIZE*currentStep)]=buffer[i]+128;
			}
			++currentStep;
		}
		free(buffer);

		//Apply the key
		int8_t *outRes=modifyAudioData( audioData, numSamples, key, keySize );

		//Verificar nombre de archivo de salida
		char outName[1024];
		if( itHasExt( outFilename, ".wav" ) == 0 )
		{
			strcpy(outName, outFilename);
			strcat(outName, ".wav");
		}
		else
		{
			strcpy(outName, outFilename);
		}

		//Write to wav file
		writeAudioFile( outName, outRes, &wavHeader, numSamples );


		//Write key to file
		char outKeyName[ strlen(outName)-4  + strlen(".toch") + 1];
		memset(outKeyName, '\0', sizeof(char) * strlen(outName)-4  + strlen(".toch") + 1 );
		strncpy( outKeyName, outName, ( strlen(outName) - 4 ) );
		strcat( outKeyName, ".toch" );

		printf("STRING:%s\n", outKeyName);

		//Create key file
		printEnc( key, keySize, outKeyName );

		//Liberar memoria
		free (audioData);
		free(outRes);
	}
	fclose(wavFile);

	//Remover archivo temporal creado
	char commandRM[1024];
	strcpy(commandRM, "rm -f " );
	strcat(commandRM, wavFilename );
	//system(commandRM);
}

//Proceso de decryptacion de una wav file
int decryptSeqProc(char *filename, char *keyFile, char *outFilename )
{
	wav_hdr wavHeader;
	int headerSize = sizeof(wav_hdr), filelength = 0;

	FILE* wavFile = fopen( filename, "r");
	if ( wavFile == nullptr )
	{
		fprintf(stderr, "Unable to open wave file: %s\n", filename );
		return 1;
	}

	//Read the header
	size_t bytesRead = fread(&wavHeader, 1, headerSize, wavFile);

	if (bytesRead > 0)
	{


		int currentStep=0;

		//Read the data
		uint16_t bytesPerSample = wavHeader.bitsPerSample / 8;      //Number     of bytes per sample
		uint64_t numSamples = wavHeader.ChunkSize / bytesPerSample; //How many samples are in the wav file?

		//Get key size from file
		int keySize=(int)sqrt(tochSize(keyFile));
	printf("Pko %s\n", keyFile);

		//Reserve memory for temporal array
		int *arr=(int*)malloc(sizeof(int) * keySize*keySize);

		//Decrypt the matrix
		tochDecrypt( keyFile , arr);

		//Convert the vector into a matrix so we can get the inverse matrix
		int **orgKey=key1Dto2D( arr, keySize);

		//Get the inverse matrix
		int *key=getInverseKey( orgKey, keySize, MOD);


		//Refill in case the size of the key does not fit
		if( numSamples % keySize != 0 )
		{
			numSamples=(keySize-(numSamples % keySize) ) + numSamples;
		}


		//Tamano del buffer
		static const uint16_t BUFFER_SIZE = 4096;
		uint64_t nSamples=0;

		//Crear llave que sea multiplo del tamano de la llave
		if( numSamples % keySize != 0 )
		{
			nSamples=(keySize-(numSamples % keySize) ) + numSamples;
		}
		else
		{
			nSamples=numSamples;
		}

		//Reservar memoria para los datos del audio
		uint8_t *audioData = (uint8_t*)malloc( sizeof(uint8_t) * bytesPerSample * nSamples);

		//Avoid memory problems, set all to 0(clean)
		memset( audioData, 0, sizeof(uint8_t) * nSamples );

		//Buffer para ir guardando los datos de audio
		int8_t* buffer =  (int8_t*)malloc(sizeof(int8_t) * BUFFER_SIZE);

		//Store data
		while (  ( bytesRead = fread(buffer, sizeof buffer[0], BUFFER_SIZE / (sizeof buffer[0]), wavFile)  ) > 0  )
		{
			for( int i=0; i<bytesRead; i++ )
			{
				audioData[i+(BUFFER_SIZE*currentStep)]=buffer[i]+128;
			}
			++currentStep;
		}
		free(buffer);


		//Aplicar llave al audio
		int8_t *outRes=modifyAudioData( audioData, numSamples, key, keySize );

		//Escribir todos los datos en un archivo binario(wav)
		writeAudioFile( outFilename, outRes, &wavHeader, numSamples );

		//Free memory
		free (audioData);
		free(outRes);

		//Output file mp3
		char outFlac[1024];
		if( itHasExt( outFilename, ".wav" ) == 0 )
		{
			//Copiar el string tal cual, ya que no tiene .wac
			strcpy( outFlac, outFilename );
			strcat( outFlac, ".flac");
		}
		else
		{
			//Copiar hasta donde no tiene .wav
			strncpy(outFlac, outFilename, strlen(outFilename)-strlen(".wav") );
			strcat( outFlac, ".flac");
		}

		//Create the command for ffmpeg, this will help to make a generic input for the algorithm
		char command[3000];
		memset( command, '\0', 3000 );
		strcpy(command, "ffmpeg -i ");
		strcat(command, outFilename );
		strcat(command, "  ");
		strcat( command, outFlac );
		system( command );

		//Remove created wav file
		char commandRM[3000];
		memset( commandRM, '\0', 3000 );
		strcpy( commandRM, "rm -f " );
		strcat( commandRM, outFilename );
		system( commandRM );
	}

	fclose(wavFile);
	return 0;
}

int8_t *modifyAudioData( uint8_t *data, uint64_t samples, int *key, uint16_t keySize )
{
	int8_t *result=( int8_t* ) malloc(sizeof(int8_t) * samples);
	int subResult[keySize];
	int fullKeySize=keySize*keySize;
	int j=0, q=0;
	uint8_t *currentData=NULL;

	for( uint64_t i=0; i<samples; ++i )
	{
		for( q=0; q<keySize; q++ )
		{
			subResult[q]=0;
		}

		currentData=data+i;
		matrixMult(subResult, currentData, key, keySize );

		for( j=0; j<keySize; j++ )
		{
			result[i+j]=(subResult[j]%MOD)-128;
		}
		i+=keySize-1;
	}
	return result;
}

void writeAudioFile( const char *outName, int8_t *data, wav_hdr *header, uint64_t samples )
{
	FILE* out = fopen( outName, "w");

	//Write header on file
	printf("Writing header...\n");
	fwrite( header, sizeof(wav_hdr), 1, out);

	//Write audio data
	fwrite( data, sizeof(int8_t), samples, out );

	fclose(out);
	return;
}

// find the file size
int getFileSize(FILE* inFile)
{
    int fileSize = 0;
    fseek(inFile, 0, SEEK_END);

    fileSize = ftell(inFile);

    fseek(inFile, 0, SEEK_SET);
    return fileSize;
}


int main( int argc, char *argv[] )
{
	//Encrypt
	if( argc == 3 )
		encryptSeqProc( argv[1], argv[2] );
	//Decrypt
	else if( argc == 4 )
		decryptSeqProc( argv[1], argv[2], argv[3] );
	return 0;
}
