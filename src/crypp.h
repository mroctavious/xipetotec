//This package will create a random password and will write it in a file
//###Algorithmo de cifrado de posicion cerrada!
//##Alias: Algorithmo de la pista del conejo
//Print on file the encrypted password,
void printEnc( int  *KEY, char *NAME);

//Get random character for the algorithm
char getChar(void);

//Get a random Spetial character for the algorithm
char getSpecialChar(void);

//Decrypt an input file
void decrypt(char *FILE_DIR);

//This function will calculate the encryption steps used, will return an integer number
int tochLines(char *FILE_DIR);

//Go through all the file and decrypt line per line,
//all will be saven in the ptr FULL_KEY
float *tochDecrypt(char *FILE_DIR, int* FULL_KEY, int NUMBER_OF_LINES);

//Decrypt a string given and will save it in an array
void decrypt(char *ENCRYPTED_STRING, int *TMP_KEY, int LENGTH_OF_STRING);

//Print matrix with spetial message
void printM(int *KEYS, char *MSG);

//Print all the keys inside the matrix
void printFk(int *KEYS, char *MSG, int NUMBER_OF_KEYS);

//Print a file with the passwords encrypted
void printEnc(int *KEY, FILE *OUTPUT_FILE);

//Copiar llave temporal a la matriz de llaves
void copyKey(int *KEY, int *TMP_KEY, int INDEX_OF_FULL_KEY);

//Copia el contenido de la llave en una nueva matriz temporal,
//es para manipular matrices de tamano 9 cada iteracion
void keyToCopy(int *KEYS, int INDEX, int *OUTPUT_ARRAY);

//This function wll give format from string to
//the values we need for the video format info
//Needs to be freed  free(result)
float *getVideoInfo (char *STRING_INFORMATION);

//This function will receibe an array of strings and will sort them
void strSort(int NUMBER_OF_ELEMENTS, char *FILENAMES[]);
int getEncNum(void);

//Include the funtions
#include "crypp.c"
