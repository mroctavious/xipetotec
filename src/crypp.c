//###Algorithmo de cifrado de posicion cerrada!
void printM(int *keys, char *msg)
{
	int i,j;
	printf("//////Printing %s ////////\n", msg );
	for(i=0; i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("%d ", keys[(i*3)+j]);
		}
		printf("\n");
	}
};

void printFk(int *keys, char *msg, int lines)
{
	int i,j;
	printf("//////Printing %s ////////\n", msg );
	for(i=0; i<lines;i++)
	{
		for(j=0;j<9;j++)
		{
			printf("Keys[%d]\t%d\n ", (i*9)+j, keys[(i*9)+j]);
		}
		printf("\n");
	}
};

char getSpecialChar()
{
	int myRand=rand() % 37;
	if ( myRand > 31 )
	{
		return (char) myRand;
	}
	else
	{
		return getSpecialChar();
	}
}

char getChar()
{
	int myRand=rand() % 127;
	if ( myRand > 36 )
	{
		return (char) myRand;
	}
	else
	{
		return getChar();
	}
}

void printEnc(int *key, int size,  char *outfile)
{
	FILE *out;
	out=fopen(outfile, "w");
	printf("Output sound key:%s\n", outfile);
	int i=0;
	int keyNum=0;
	int nextNum=key[keyNum];
	while( keyNum < size*size )
	{
		char myChar=getChar();

		if( i == nextNum )
		{
			myChar=getSpecialChar();
			keyNum++;
			nextNum=key[keyNum];
			i=-1;

		}
		fprintf(out,"%c",myChar);
		//printf("%c  ",myChar);
		i++;
	}

	fprintf(out,"\n");
	fclose(out);
}


//Decrypt all the characters
int decrypt(char *line, int *tmpKey, int len)
{
	int tepoztecatl=0;
	int i;
	int j=0;
	for(i=0; i<len; i++)
	{
		char myChar=line[i];
		if (myChar < 37 && myChar > 31)
		{
			tmpKey[j]=tepoztecatl;
			j++;
			tepoztecatl=0;
		}
		else
		{
			tepoztecatl++;
		}
	}
	return j;
}

//Copiar llave temporal a la matriz de llaves
void copyKey(int *key, int *tmpKey, int index)
{
	int i;
	for(i=0;i<9;i++)
	{

		int myIndex=(index*9) + i;
		key[myIndex]=tmpKey[i];

	}
}

//Copia el contenido de la llave en una nueva matriz temporal,
//es para manipular matrices de tamano 9 cada iteracion
void keyToCopy(int *keys, int index, int *outKey)
{
	int i;
	for(i=0;i<9;i++)
	{
		int myIndex=(index*9) + i;
		outKey[i]=keys[myIndex];
	}
}


//Get the number of lines inside the file
int tochLines(char *file)
{
	FILE *Archivo;
	char *line = NULL;
	size_t len = 0;
	//ssize_t read;
	int lineNum=0;
	Archivo = fopen(file, "r");
	if (Archivo == NULL)
	{
		printf("Could not open key file %s.\n", file);
		exit(EXIT_FAILURE);
	}


	//while ( (read = getline(&line, &len, Archivo)) != -1 )
	while ( (getline(&line, &len, Archivo)) != -1 )
	{
		lineNum+=1;
	}
	free(line);
	fclose(Archivo);
	return lineNum;
}

//Decrypt the inpur and save keys in a matrix
int tochSize(char *file)
{
	FILE *Archivo;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	//int index=0;
	int size=0;
	Archivo = fopen(file, "r");
	printf("Archivo llave: %s\n", file);
	if (Archivo == NULL)
	{
		printf("Error opening key file %s!\n", file);
		exit(EXIT_FAILURE);
	}

	while ( (read = getline(&line, &len, Archivo) ) != -1 )
	{
		if( read > 0 )
		{
			int tmpKey[400];
			size=decrypt( line, tmpKey, read );
		}

	}
	free(line);
	fclose(Archivo);
	return size;
}
//Decrypt the inpur and save keys in a matrix
int tochDecrypt(char *file, int *fullKey)
{
	FILE *Archivo;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	//int index=0;
	int size=0;
	Archivo = fopen(file, "r");
	if (Archivo == NULL)
	{
		exit(EXIT_FAILURE);
	}

	while ( (read = getline(&line, &len, Archivo) ) != -1 )
	{
		if( read > 0 )
		{
			int tmpKey[400];
			size=decrypt( line, tmpKey, read );
			memcpy( fullKey, tmpKey, sizeof(int) * size );
		}

	}
	free(line);
	fclose(Archivo);
	return size;
}

//This function wll give format from string to
//the values we need for the video format info
//Needs to be freed  free(result)
float *getVideoInfo (char *information)
{
	float *result=(float *) malloc(sizeof(float) * 4 );
	float fps;
	int resX;
	int resY;
	float totalFrames;
	sscanf(information,"%f %d %d %f",&fps, &resX, &resY, &totalFrames);
	result[0]=fps;
	result[1]=(float)resX;
	result[2]=(float)resY;
	result[3]=totalFrames;
	return result;
}



//Funcion que ordenara arreglo de strings
void strSort(int n, char *fileNames[])
{
	//Tmp string which will be used to swap
	char *tmp;
	int i, j;

	printf("n=%d\n",n);
	//Go through all the strings and compare them
	for(j=0; j<n-1; j++)
	{
		for(i=j+1; i<n; i++)
		{
			if(strcmp(fileNames[j], fileNames[i]) > 0)
			{
				//Swap
				tmp=fileNames[j];
				fileNames[j]=fileNames[i];
				fileNames[i]=tmp;
			}
		}
	}
	return;
}

int getEncNum(void)
{
	//int num=(int)rand() % 11;
	int num=10;
	printf("Encryptions=%d\n", num);
	if (num>1)
	{
		return num;
	}
	else
	{
		return getEncNum();
	}
}
