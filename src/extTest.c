#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int itHasExt( char *filename, char *ext )
{
	//Verify if the file has the .wav extension
	int continueFlag=0;

	//Get the size of the string, we will check +4 positions
	int len=strlen(filename);

	//Position and confirmed
	int pos=0, i, confirmed=0;

	//Extension to check
	int extLen=strlen(ext);

	//Recorrer el nombre de archivo en busca de la extension
	for( i=len-4; i<len; i++ )
	{
		//Check if the character is the same in the extension
		if( filename[i] == ext[pos] )
		{
			++confirmed;
		}
		++pos;
	}

	//If all ext characters were found
	if( confirmed == extLen )
		return 1;
	else
		return 0;
}

int main(int argc, char *argv[] )
{
	printf("%d\n", itHasExt(argv[1], ".wav"));
	return 0;
}
