#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <inttypes.h>
#include <string.h>
#include "hcTools.cpp"
#include "matrixHCFunctions.h"
#include "crypp.c"
#include "Ometilistli.cu"
#define KEY_DEFAULT_SIZE 9
#define MOD 256

//Compiling g++ -std=c++11
using std::cin;
using std::cout;
using std::endl;
using std::fstream;
using std::string;

typedef struct  WAV_HEADER
{
    //RIFF Chunk Descriptor
    uint8_t         RIFF[4];        // RIFF Header Magic header
    uint32_t        ChunkSize;      // RIFF Chunk Size
    uint8_t         WAVE[4];        // WAVE Header

    //FMT sub-chunk
    uint8_t         fmt[4];         // FMT header
    uint32_t        Subchunk1Size;  // Size of the fmt chunk
    uint16_t        AudioFormat;    // Audio format 1=PCM,6=mulaw,7=alaw,     257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
    uint16_t        NumOfChan;      // Number of channels 1=Mono 2=Sterio
    uint32_t        SamplesPerSec;  // Sampling Frequency in Hz
    uint32_t        bytesPerSec;    // bytes per second
    uint16_t        blockAlign;     // 2=16-bit mono, 4=16-bit stereo
    uint16_t        bitsPerSample;  // Number of bits per sample

    //Data sub-chunk
    uint8_t         Subchunk2ID[4]; // "data"  string
    uint32_t        Subchunk2Size;  // Sampled data length
} wav_hdr;


// Function prototypes
// find the file size
int getFileSize(FILE* inFile);

//Write the result wav file
void writeAudioFile( const char *outName, int8_t *data, wav_hdr *header, uint64_t samples );

//Apply the key to the vector
int8_t *modifyAudioData( uint8_t *data, uint64_t samples, int *key, uint16_t keySize );

//Temporal function to find a key size multiple of the size of the vector
//Jaime's formula is better to use
int getKeySize( uint64_t nSamples, int keySize );

//Check if the string has the extension
int itHasExt( char *filename, const char *ext );

//Sequencial process to encrypt an audio file with the WAV format, it can receive any audio format
int encryptSeqProc( char *filename, char *outFilename );

//Proceso de decryptacion de una wav file
int decryptSeqProc(char *filename, char *keyFile, char *outFilename );

#include "HCAudioTools.cpp"
