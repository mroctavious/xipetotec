#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

/*
   Recursive definition of determinate using expansion by minors.
*/
int Determinant(int **a,int n)
{
   int i,j,j1,j2;
   int det = 0;
   int **m = NULL;

   if (n < 1) { /* Error */

   } else if (n == 1) { /* Shouldn't get used */
      det = a[0][0];
   } else if (n == 2) {
      det = a[0][0] * a[1][1] - a[1][0] * a[0][1];
   } else {
      det = 0;
      for (j1=0;j1<n;j1++) {
         m = malloc((n-1)*sizeof(int *));
         for (i=0;i<n-1;i++)
            m[i] = malloc((n-1)*sizeof(int));
         for (i=1;i<n;i++) {
            j2 = 0;
            for (j=0;j<n;j++) {
               if (j == j1)
                  continue;
               m[i-1][j2] = a[i][j];
               j2++;
            }
         }
         det += pow(-1.0,j1+2.0) * a[0][j1] * Determinant(m,n-1);
         for (i=0;i<n-1;i++)
            free(m[i]);
         free(m);
      }
   }
   return(det);
}

/*
   Find the cofactor matrix of a square matrix
*/
void CoFactor(int **a,int n,int **b)
{
   int i,j,ii,jj,i1,j1;
   int det;
   int **c;

   c = malloc((n-1)*sizeof(int *));
   for (i=0;i<n-1;i++)
     c[i] = malloc((n-1)*sizeof(int));

   for (j=0;j<n;j++) {
      for (i=0;i<n;i++) {

         /* Form the adjoint a_ij */
         i1 = 0;
         for (ii=0;ii<n;ii++) {
            if (ii == i)
               continue;
            j1 = 0;
            for (jj=0;jj<n;jj++) {
               if (jj == j)
                  continue;
               c[i1][j1] = a[ii][jj];
               j1++;
            }
            i1++;
         }

         /* Calculate the determinate */
         det = Determinant(c,n-1);

         /* Fill in the elements of the cofactor */
         b[i][j] = pow(-1.0,i+j+2.0) * det;
      }
   }
   for (i=0;i<n-1;i++)
      free(c[i]);
   free(c);
}

/*
   Transpose of a square matrix, do it in place
*/
void Transpose(int **a,int n)
{
   int i,j;
   int tmp;

   for (i=1;i<n;i++) {
      for (j=0;j<i;j++) {
         tmp = a[i][j];
         a[i][j] = a[j][i];
         a[j][i] = tmp;
      }
   }
}


//Euclidan module
int mymod (int n, int m)
{
	if( n < 0 )
	{
		if ( ((n % m) + m) == m )
		{
			return 0;
		}
		else
		{
			return ((n % m) + m);
		}
	}
	else
	{
		return n % m;
	}
}


//This function will calculate if its an integer number
bool isInt(double number)
{
	int integer=(int) number;
	return (number == integer);
}

//Modular Multiplicative Inverse
int modular_inverse_multiplicative(int determinant, int mod)
{
	///////////////////////////////////////
	//FORMULA//////////////////////////////
	//	X=( ( MODULE * i) + 1 ) / R  //
	///////////////////////////////////////
	//printf("modular_inverse_multiplicative now1\nDeterminant:%d\n",determinant);

	//Iterator variable
	int i;
	//printf("modular_inverse_multiplicative now2\n");
	//Get the euclidan module of the determinant and save it in r, this will be used to get the inverse(part of the formula).
	int r=mymod(determinant, mod);
	//printf("modular_inverse_multiplicative now3\n");

	//Applying brute force to get the number which will feet to the first INTEGER number
	for(i=1; i<9457; i++)
	{
		//Apply the formula
		double x=( ( (double) mod * (double) i ) + 1 ) / (double) r;

		//printf("x=( ( %d * %d ) + 1 ) / %d\n",MODULE,i,r);
		//printf("x=%f\n",x);

		//Check if its an integer number
		if(isInt(x) == true)
		{
			return (int) x;
		}
	}
	return 0;


}

int *applyModular(int **a, int n, int det, int mod)
{
	int *inverse_array=(int*)malloc( sizeof(int) * n*n );
	int i,j;
	for( i=0; i<n; i++ )
		for( j=0; j<n; j++)
			a[i][j]=mymod(a[i][j], mod);

	int mim=modular_inverse_multiplicative(det, mod);
	for(i=0;i<n;i++)
	{
		for( j=0; j<n; j++)
		{
			inverse_array[i*n+j]=mymod((mim * a[i][j]), mod );
		}
	}
	return inverse_array;

}


int main(int argc, char **argv )
{
	int N=4;
        int **Arr =(int **)malloc( sizeof(int*) * N );
        int **Barr =(int **)malloc( sizeof(int*) * N );
	int i=0;
	for( i=0; i<N; i++ )
	{
		Arr[i]=(int*)malloc(sizeof(int) * N );
		Barr[i]=(int*)malloc(sizeof(int) * N );
	}

/*	Arr[0][0]=17;
	Arr[0][1]=17;
	Arr[0][2]=5;
	Arr[1][0]=21;
	Arr[1][1]=18;
	Arr[1][2]=21;
	Arr[2][0]=2;
	Arr[2][1]=2;
	Arr[2][2]=19;
*/
	Arr[0][0]=14;
	Arr[0][1]=17;
	Arr[0][2]=28;
	Arr[0][3]=13;
	Arr[1][0]=18;
	Arr[1][1]=46;
	Arr[1][2]=12;
	Arr[1][3]=11;
	Arr[2][0]=192;
	Arr[2][1]=83;
	Arr[2][2]=91;
	Arr[2][3]=15;
	Arr[3][0]=11;
	Arr[3][1]=23;
	Arr[3][2]=29;
	Arr[3][3]=109;

	printf("Det=%d\n", Determinant( Arr, N) );

	CoFactor( Arr, N, Barr);

	for( i=0; i<N; i++ )
	{
		for( int j=0; j<N; j++ )
			printf("%d\t", Barr[i][j]);
		printf("\n");
	}

	//Adjunta
	Transpose(Barr, N);
	printf("Transpose:\n\n");
	for( i=0; i<N; i++ )
	{
		for( int j=0; j<N; j++ )
			printf("%d\t", Barr[i][j]);
		printf("\n");
	}
	printf("\n\n\n");
	int *inv=applyModular(Barr, N, Determinant( Arr, N), 26);
	printf("With modular:\n");
	for( i=0; i<N; i++ )
	{
		for( int j=0; j<N; j++ )
			printf("%d\t", inv[i*N+j]);
		printf("\n");
	}
	for( i=0; i<N; i++ )
	{
		free(Arr[i]);
		free(Barr[i]);
	}

	free(inv);
	free(Arr);
	free(Barr);
	return 0;
}
