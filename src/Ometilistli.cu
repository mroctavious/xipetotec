//  _______  _______  _______ __________________ _       _________ _______ _________ _       _________
// (  ___  )(       )(  ____ \\__   __/\__   __/( \      \__   __/(  ____ \\__   __/( \      \__   __/
// | (   ) || () () || (    \/   ) (      ) (   | (         ) (   | (    \/   ) (   | (         ) (
// | |   | || || || || (__       | |      | |   | |         | |   | (_____    | |   | |         | |
// | |   | || |(_)| ||  __)      | |      | |   | |         | |   (_____  )   | |   | |         | |
// | |   | || |   | || (         | |      | |   | |         | |         ) |   | |   | |         | |
// | (___) || )   ( || (____/\   | |   ___) (___| (____/\___) (___/\____) |   | |   | (____/\___) (___
// (_______)|/     \|(_______/   )_(   \_______/(_______/\_______/\_______)   )_(   (_______/\_______/
//
//
//
//
//


/*----------------------------------------------------------------------------------------

    	*Se maneja un solo arreglo el cual mediante apuntadores se delimitara
   dependiendo de la cantidad de particiones a realizar

 	*El arreglo se adapta tanto al tamaño de la llave como a la cantidad de devices
   para asi obtener un comun multiplo de ambos

	*Super formula nextInt = (cM - (tamAudio % cM)) + tamAudio;
	<<El comun multiplo esta dado por cM = (tamAudio * tamLlave)>>

	*Se cambio de estrategia -> un bloque bidimensional de hilos

	*Se creo la funcion ometilistli la cual engloba todo

	*Se hicieron los cambios de tipo de variable para el vector resultante

	*Se comentaron los printf (print Multi linea 70)

-----------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <time.h>
#include <pthread.h>

#define KEY_MAX_SIZE 3
#define MOD 256

__global__ void cudaCrypt8( uint8_t *newVect, int *key, int8_t *resultado, int keySize, long long int newVectSize){

        extern __shared__ int cache[];//Determina que es memoria que solo conocen los hilos del mismo bloque y se genera la variable cache por cada conjunto(Lo realiza el primer hilo en llegar)

	//Index para el elemento del vector
        int vectId = 0;
	//Index para la posicion del elemento de la llave
	int keyId = 0;

	int mod = 0;

			//Indexacion para la llave bidimensional
			keyId = ((threadIdx.x * keySize)+ threadIdx.y);

			//Indexacion para el vectorAudio
			vectId = ((blockIdx.x * keySize) + threadIdx.y);

			//Se realiza la multiplicacion de matrices renglon * columna
			mod = (key[(keyId)] * newVect[(vectId)]);

			//Se realiza una variable cache por cada bloque de hilos
			cache[(threadIdx.x * keySize) + threadIdx.y] = mod;

//		printf("\nMultiKernel[%d]: (%d) * (%d) -> %d\n", blockIdx.x, key[keyId], newVect[vectId], cache[threadIdx.x]);

       __syncthreads();//Se sicronizan los hilos
        //Una vez que todos los hilos terminan se puede realizar la suma



        //Se va recorriendo el cache
	if (threadIdx.y  == 0)  // soy el hilo 0
        {
                int suma = 0;
		int j = 0;

                for ( j = 0; j < keySize; j++){

			//Realizamos la sumatoria de la cantidad de numeros correspondidos por el tamLlave(numColumnas) para
			//despues asignar el valor a la posicion que le corresponde al arregle resultado
			suma += cache[(threadIdx.x * keySize) + j];

                }

		//El resultado se almacenara en la posicion que indique su propia indexaxion, antes de almacenarlo se aplicara un mod 256
		resultado[blockIdx.x*blockDim.x+threadIdx.x] = (suma % 256)-128;
		//printf("Resultado %d -> %d\n", blockIdx.x*blockDim.x+threadIdx.x, resultado[blockIdx.x*blockDim.x+threadIdx.x]);
	}

}

//Reservaciones y asiganaciones de memoria para la GPU
//Pasamos los punteros como referencia (*&ptr) para de esta forma poder modificar a que direccion de memoria apunta
void cudaMem(uint8_t *&dev_audio, int *&dev_Llave, int8_t *&dev_resultado, long long int nuevoTamVect, int tamIave){

        int tamLlave = (tamIave * tamIave);

        //Reservamos memoria en la GPU
        cudaMalloc((void**)&dev_audio, sizeof(uint8_t)*nuevoTamVect);
        cudaMalloc((void**)&dev_Llave, sizeof(int)*(tamLlave));
        cudaMalloc((void**)&dev_resultado, sizeof(int8_t)*nuevoTamVect);

        //Inicializamos la memoria en la GPU con 0
        cudaMemset(dev_audio, 0, sizeof(uint8_t)*nuevoTamVect);
        cudaMemset(dev_Llave, 0, sizeof(int)*(tamLlave));
        cudaMemset(dev_resultado, 0, sizeof(int8_t)*nuevoTamVect);

	if(dev_audio == NULL || dev_Llave == NULL || dev_resultado == NULL){

		printf("\nError en cudaMalloc y memset, tamano:%lld\n", nuevoTamVect);

	} /* else{

		printf("\nMemory OK\n");

	} */

}

void cudaCpyHostDevice(uint8_t *dev_audio, int *dev_Llave, uint8_t *audioExt, int *Llave, long long int nuevoTamVect, int tamIave){

        int tamLlave = (tamIave * tamIave);

        //Copiamos los valores del vector de AudioOriginal y de la Llave en el CPU al GPU
        cudaMemcpy(dev_audio, audioExt, sizeof(uint8_t)*nuevoTamVect, cudaMemcpyHostToDevice);
        cudaMemcpy(dev_Llave, Llave, sizeof(int)*(tamLlave), cudaMemcpyHostToDevice);

}


//Estructura para los hilos del CPU
typedef struct hiloCPU{

	//Tamaño original del vectorAudio
	long long int tamActual;

	//Tamaño redefinido del vectorAudio
	long long int tamRedefinido;

	//Inicio y final de cada particion
	long long int inicio;
	long long int final;

	//Espacio a recorrer
	long long unsigned int brinco;

	//Identificador del hilo
	int hiloID;

	//Arreglo del Audio(particionado) y llave
	uint8_t *Arreglo;
	int *key;
	int keySize;

	//ArregloExtendido
	void *arrExt;

	int8_t *result;

	//Memoria para la GPUinfHilo.Arreglo
	int8_t *devResult;
	uint8_t *devauVect;
	int *devKey;

}HiloCPU;

void crearHilo(HiloCPU *arrHilo, int numDev){

	//HiloCPU *newHiloCPU;
	//newHiloCPU  = (HiloCPU*)malloc(sizeof(HiloCPU)*1);

	int i = 0;

	for(i = 0; i < numDev; i++){

		//Tamaño original del vector
		arrHilo[i].tamActual = 0;

		//Tamaño redefinido del vector
		arrHilo[i].tamRedefinido = 0;

		//Tamaño de la llave
		arrHilo[i].keySize = 0;

		//Punto inicial de la particion
		arrHilo[i].inicio = 0;

		//Punto final de la particion
		arrHilo[i].final = 0;

		//Tamaño de la particion (brinco que da entre parte y parte)
		arrHilo[i].brinco = 0;

		//Hilo del cpu que se encarga
		arrHilo[i].hiloID = 0;

		//Direccion de memoria de la particion correpondiente
		arrHilo[i].Arreglo = NULL;

		//Llave con la que se trabaja
		arrHilo[i].key = NULL;

		//Arreglo extendido
		arrHilo[i].arrExt = NULL;

		//Resultado multiplicacion
		arrHilo[i].result = NULL;

		//Memoria para la GPU
		arrHilo[i].devResult = NULL;
		arrHilo[i].devauVect = NULL;
		arrHilo[i].devKey = NULL;

	}
}


//Creamos un arreglo con los suficientes espacios para las estructuras que sean necesarias
HiloCPU *crearArreglo(int numDev){

//Asignamos el espacio en la memoria que sea necesario
HiloCPU *arrHilos = (HiloCPU*)malloc(sizeof(HiloCPU)*numDev);

//Se crea un hilo de CPU por cada GPU que se encuentre
crearHilo(arrHilos, numDev);

return arrHilos;

}


//Obtener los devices disponibles en el equipo
int getDev(){

	int numDev;
	//Obtenemos la cantidad de dispositivos
	cudaGetDeviceCount(&numDev);

	return numDev;

}

//Imprimimos las estructuras dentro del arreglo de estructuras
void pruebaArreglo(HiloCPU *arreglo, int numDev){

	int i = 0;
	for(i = 0; i < numDev; i++){


		//printf("\nEstructura %d: inicio->%d final->%d memoDir->%d brinco->%d\n", i, arreglo[i].inicio, arreglo[i].final, arreglo[i].Arreglo, arreglo[i].brinco);

		//printf("Direccion memo: %d", &audio[i*particion]);

	}

}



//Delimitaciones de las partes del vector que le corresponde a cada dispositivo
void delimitarVect(uint8_t  *audio, long long int tam, int *llave, HiloCPU *array, int devCount){

	//int devCount;
	int sobrantes = 0;

	//Division de los elementos
	int particion = tam / devCount;

	//Delimitaciones de las partes
	int i = 0;
	int j = 0;
	long long int pivote = 0;

	if(tam % devCount != 0){

		//printf("\n\n!!La particion sera desigual debido a la cantidad de elementos y devices!!\n\n");

	}else{

		//printf("\n\nParticion exacta debido a la cantidad de elementos y devices!!\n\n");

	}


	for(i = 0; i < devCount; i++){

		//printf("Num de elementos para el Dev %d: %d -> %d\n", i, pivote, ((i+1) * particion)-1);
		pivote = ((i+1) * particion);

		array[i].inicio = 0;
		//Se resta 1 ya que vamos en secuencia desde 0
		array[i].final = (particion - 1);

		array[i].hiloID = i;

		//ptr =  &audio[i*particion];

		//printf("Posicion: %d", i*particion);
		//printf("\tDireccion memo: %d", &audio[i*particion]);
		//printf("\nImprimiendo elemento: %d\n", *ptr);
		//printf("\nFINAL: %d\n", array[i].final);

		array[i].Arreglo = &audio[i*particion];
		array[i].key =  llave;
		array[i].brinco = particion;
	}

	sobrantes = (tam-1) - pivote;

	if(sobrantes > 0){

		//printf("\nElementos sin device asignado\n");
		for(j = 0; j <= sobrantes; j++){

			//printf("Elememto: %d, ", pivote);
			pivote++;
		}

	//printf("\n");


	}


}

//Calculo de los bloques por Device
int calcBloques(long long int tamAudio, int tamLlave, int devCount){

	//Division de Device
	int parcial;
	parcial = tamAudio / devCount;
	int totalBloques = 0;
	//El total de bloques sera definido por el tamaño de la llave
	totalBloques = (parcial / (tamLlave) + 1);

	return totalBloques;

}

//Calculo de hilos por Device
int calcHilos(int tamIave){

	int totalHilos = 0;
	//La cantidad de hilos sera determinado por el numero de columnas de la llave
	totalHilos = tamIave;

	return totalHilos;
}


//Rellenamos de valores aleatorios el vector
void llenarVect(uint8_t *audioTest, int tam){


	//Creamos un vector
	int i = 0;
	srand((unsigned)time(NULL)); //semilla de aleatorios dinamica

        //fill arrays audioTest
        for (i = 0; i<tam; i++){

		int r = (rand() % 255) + 1;

		audioTest[i] = r;

		//printf("llaveTest[%d]: %d\n", i, r);

        }

}

//Rellenamos de valores aleatorios el vector
void llenarLlave(int *llaveTest, int tam){


	//Creamos un vector
	int i = 0;
	srand((unsigned)time(NULL)); // semilla de aleatorios dinamica

        //fill arrays audioTest
        for (i = 0; i < (tam*tam); i++){

		int r = (rand() % 255) + 1;

		llaveTest[i] = r;

		//printf("llaveTest[%d]: %d\n", i, r);

        }

}



//Se crea vectorAudio
uint8_t *crearVect(int tamVect){

uint8_t *test;

test = (uint8_t *)malloc(sizeof(uint8_t)*tamVect);
memset(test, 0, sizeof(uint8_t)*tamVect);

return test;

}

//Se crea vector resultado
int8_t *crearVectResu(int tamVect){

int8_t *test = NULL;

test = (int8_t *)malloc(sizeof(int8_t)*tamVect);
memset(test, 0, sizeof(int8_t)*tamVect);

return test;

}


//Se crea vectorLlave
int *crearLlave(int tamVect){

int *test;

test = (int *)malloc(sizeof(int)*(tamVect*tamVect));
memset(test, 0, sizeof(int)*(tamVect*tamVect));

return test;

}


void printArray(int tam, uint8_t *array){

	int i = 0;

	//printf("Impriminendo arreglo\n");

	for(i = 0; i < tam; i++){

		//printf("Array[%d] = %d\n", i, array[i]);
	}

}


void printArrayResul(int tam, int *array){

	int i = 0;

	//printf("Impriminendo arreglo\n");

	for(i = 0; i < tam; i++){

		//printf("Array[%d] = %d\n", i, array[i]);
	}

}


//Obtenemos un comun multiplo entre la llave y la cantidad de Devices para posteriormente
//Reasignar el tamaño del vector de audio para que sea valida la multiplicacion.
int comunMultiplo(int tamIave, int devCount){

	int multi = 0;

	multi = (devCount * tamIave);

	return multi;

}

bool extendIes(long long int tamAudio, int tamIave, int devCount){

	//seHacelaCarnita = true -> se necesita extender
	//seHacelaCarnita = false -> no se necesita extender

	bool seHacelaCarnita = false;

	int div = 0;

	div = tamAudio / devCount;

	if(tamAudio % devCount != 0){

		seHacelaCarnita = true;

	}else if(div % tamIave != 0){

		seHacelaCarnita = true;
	}

	return seHacelaCarnita;

}


//Se verifica si la multiplicacion entre la llave y la particion es posible
//Si no lo es naturalmen*sizeof(int)nte, se transforma el tam del audio hasta el siguiente multiplo para forzar la realizacion
uint8_t *sePuede(int tamIave, long long int tamAudio, uint8_t *audio, int devCount, HiloCPU *array){

//	uint8_t *iave2 = NULL;
	uint8_t *vectAudio2 = NULL;
	bool seHace;

	//printf("\nIntento de multiplicacion: Llave x particion\n");

	//Se verifica si por el tamaño de la llave y de la particion se puede realizar la multiplicacion de matrices
	seHace = extendIes(tamAudio, tamIave, devCount);


	if( seHace == true ){

		//printf("\nLa particion se extendera hasta el siguiente multiplo\n");

		//Se crea un nuevo arreglo el cual, si es necesario extender, remplazara al original
		//Obtenemos la extension del nuevo vector
		long long int extVect = 0;
		int cM = comunMultiplo(tamIave, devCount);

		//Se obtiene un comun multiplo entre la Llave y la cantidad de GPUs
		extVect = (cM - (tamAudio%cM)) + tamAudio;


		printf("PKO Formula vector ext: %d\n", extVect);
		//Reservamos memoria en el arreglo extendido e inicializamos con 0
		vectAudio2 = (uint8_t *)malloc(sizeof(uint8_t) * extVect);
		memset(vectAudio2, 0, sizeof(uint8_t)*extVect);

		//Copiamos el contenido de la memoria que contenia el vector audio original al vector con
		//tamaño reasignado, los espacios sobrantes se mantendran con 0
		memcpy(vectAudio2, audio, tamAudio*sizeof(uint8_t));

		//printf("\nSe realizo la reasignacion de tamaño de arreglo\n");

		printArray(extVect, vectAudio2);

		int j = 0;
		for(j = 0; j < devCount; j++){

			//Determinamos el tamaño original y el tamaño reasignado
			array[j].tamActual = tamAudio;
			array[j].tamRedefinido = extVect;

		}

		return vectAudio2;


	}else{

		//printf("\nLa particion se puede hacer\n");

		int j = 0;
		for(j = 0; j < devCount; j++){

			//Asignamos el tamaño original, el reasignado es 0 ya que no fue necesario realizarlo
			array[j].tamActual = tamAudio;
			array[j].tamRedefinido = tamAudio;
			printf("PKO ELSE Formula vector ext: %d\n", array[j].tamRedefinido);


		}


		return audio;

	}


}

//Se imprimen los datos de las estructuras creadas
void printStruct(HiloCPU *array, int devCount){

	int i = 0;

	for(i = 0; i < devCount; i++){

		//printf("\nEstructura %d -> tamActual: %d\n", i, array[i].tamRedefinido);

	}


}

void freeAll(HiloCPU *arrPart, uint8_t *arrExt, int8_t *dev_result, int *dev_key, uint8_t *dev_auVect){

//---------------------Liberaciones----------------------
free(arrPart);
free(arrExt);
//free(test);
//free(result);

//-------------Liberaciones GPU-------------

cudaFree(dev_result);
cudaFree(dev_auVect);
cudaFree(dev_key);

}


void checkVect(uint8_t *&arrExt, int *key, long long int tamAudio, int tamIave, uint8_t *test, int numGPU, long long int *&ptrNVS, HiloCPU *arrPart, uint8_t *dev_auVect, int8_t *dev_result, int *dev_key, int hmD){

	long long int newVectSize = 0;
//	long long int totalBlocks = 0;

	arrExt = sePuede(tamIave, tamAudio, test, numGPU, arrPart);

        *ptrNVS = arrPart[0].tamRedefinido;
        newVectSize = arrPart[0].tamRedefinido;

	printf("newVectSize----> %d", newVectSize);

	delimitarVect(arrExt, newVectSize, key, arrPart, numGPU);

	//Se obtiene total bloques por GPU
	// *ptrBT = calcBloques(newVectSize, tamIave, numGPU);
	//totalBlocks = calcBloques(newVectSize, tamIave, numGPU);

	//printf("-> %d", totalBlocks);

	//resu = crearVectResu(newVectSize);
	int i = 0;
	for(i = 0; i < numGPU; i++){

		arrPart[i].keySize = tamIave;
		arrPart[i].arrExt = arrExt;

	}



}


//void goCUDA(int tamLlave, uint8_t *dev_auVect, int *dev_key, uint8_t *dev_result, long long int newVectSize, uint8_t *result){
void *goCUDA(void * hilito){

	cudaDeviceProp prop;
	int devCount = getDev();
	HiloCPU *temp = (HiloCPU *)hilito;
	HiloCPU infHilo = *temp;


	cudaSetDevice(infHilo.hiloID);
	cudaGetDeviceProperties(&prop, infHilo.hiloID);
	//printf("Soy el hilo %d %s \n",infHilo.hiloID, prop.name);

	long long int bloquesTotales = 0;
	long long int tamAudio = infHilo.tamRedefinido;
	int tamIave = infHilo.keySize;


	//Para medir el tiempo de ejecucion
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	//--------------------------------GPU---------------------------------
	cudaMem(infHilo.devauVect, infHilo.devKey, infHilo.devResult, tamAudio, tamIave);
	cudaCpyHostDevice(infHilo.devauVect, infHilo.devKey, infHilo.Arreglo, infHilo.key, infHilo.brinco, tamIave);
	//--------------------------------------------------------------------


	bloquesTotales = calcBloques( tamAudio, tamIave, devCount );

	dim3 dimGrid(bloquesTotales);
	dim3 dimBlock(tamIave, tamIave);

	//Empezar a tomar el tiempo
	cudaEventRecord(start);

	//Ejecutar el kernel paralelo
	cudaCrypt8 << <dimGrid, dimBlock, sizeof(int)*(tamIave*tamIave)>> >(infHilo.devauVect, infHilo.devKey, infHilo.devResult, tamIave, infHilo.brinco);
	cudaDeviceSynchronize();

	//Parar el tiempo
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);

	//Convertir tiempo en MS
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);

	//Imprimir tiempo de la gpu
	printf("%s: %f\n", prop.name, milliseconds);

	//-------------Device To Host-------------
	cudaMemcpy( infHilo.result + (infHilo.brinco * infHilo.hiloID), infHilo.devResult, sizeof(int8_t)*infHilo.brinco, cudaMemcpyDeviceToHost);

	return NULL;

}


int8_t *ometilistli(int *key, int tamLlave, uint8_t *test, long long int tamAudio){

//-----VARIABLES CPU------
//Arreglo de estructuras
HiloCPU *arrPart;

//Reasignacion del vector
uint8_t *arrExt = NULL;

//Resultados para el CPU
int8_t *result = NULL;


//Obtenemos cuantas GPU se encuentran disponibles
int numGPU = 0;
numGPU = getDev();

//Arreglo de Estructuras  ---> MALLOC
arrPart = crearArreglo(numGPU);

//almacena la reasignacion de tamaño
long long int newVectSize = 0;
long long int *ptrNewVectSize = NULL;

ptrNewVectSize = &newVectSize;

//Almacena la cantidad de bloques con los que se trabajara
//long long int bloquesTotales = 0;
//long long int *ptrBT = &bloquesTotales;


//------VARIABLES GPU----
int8_t *dev_result = NULL;
uint8_t *dev_auVect = NULL;
int *dev_key = NULL;

//--------------------------Aqui empíeza el pedotl-------------------
checkVect(arrExt, key, tamAudio, tamLlave, test, numGPU, ptrNewVectSize, arrPart, dev_auVect, dev_result, dev_key, numGPU);
result = crearVectResu(newVectSize);
printf("newVectSize:  %lld\n", newVectSize );

//--------------------------------GPU---------------------------------
//cudaMem(dev_auVect, dev_key, dev_result, newVectSize, tamLlave);
//cudaCpyHostDevice(dev_auVect, dev_key, arrExt, key, newVectSize, tamLlave);
//--------------------------------------------------------------------


//ASIGANACION DE LOS VECTORES A LA ESTRUCTURA
int dm = 0;
for(dm = 0; dm < numGPU; dm++){

	arrPart[dm].devResult = dev_result;
	arrPart[dm].devauVect = dev_auVect;
	arrPart[dm].devKey = dev_key;

	arrPart[dm].result = result;
}


//------Hilos CPU------
pthread_t hilitos[numGPU];

//Ejecutamos el kernel
int tid = 0;
for(tid = 0; tid < numGPU; tid++){

	//printf("\nSoy el hilo %d y brinco %d\n", tid, arrPart[tid].brinco);
	cudaSetDevice(tid);
	pthread_create(&hilitos[tid], NULL, goCUDA, arrPart+tid );
}

for(tid = 0; tid < numGPU; tid++){

	pthread_join(hilitos[tid], NULL);
}





//printf("\nArreglo Resultante\n");
//int l = 0;
//for(l = 0; l < tamAudio; l++){

//	printf("ResultReturned[%d] -> %d\n", l, result[l]);
//}


freeAll(arrPart, arrExt, dev_result, dev_key, dev_auVect);

return result;

}





